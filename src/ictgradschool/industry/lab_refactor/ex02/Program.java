package ictgradschool.industry.lab_refactor.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random r = new Random();
    private Canvas c = new Canvas();
    private int greenX = -1, greenY = -1;
    private ArrayList<Integer> redX = new ArrayList<>();
    private ArrayList<Integer> snakeX = new ArrayList<>();
    private ArrayList<Integer> redY = new ArrayList<>();
    private ArrayList<Integer> snakeY = new ArrayList<>();
    private int direction;
    private boolean done = false;

    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {
        for (int i = 0; i < 6; i++) {
            snakeX.add(10 - i);
            snakeY.add(10);
        }
        this.direction = 39;
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        c.setBackground(Color.white);
        add(BorderLayout.CENTER, c);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int d1 = e.getKeyCode();
                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.direction - d1) != 2) {// block moving back
                        Program.this.direction = d1;
                    }
                }
            }
        });
        setVisible(true);
    }


    void go() { // main loop
        while (!done) {
            int x2 = snakeX.get(0); //gets the first position of the snake
            int y2 = snakeY.get(0);
            if (direction == 37) { //move left
                x2--;
            }
            if (direction == 39) { //move right
                x2++;
            }
            if (direction == 38) { //move up
                y2--;
            }
            if (direction == 40) { //move down
                y2++;
            }
            if (x2 > 30 - 1) { //when snake hits edge, reappear on other side
                x2 = 0;
            }
            if (x2 < 0) {
                x2 = 30 - 1;
            }
            if (y2 > 20 - 1) {
                y2 = 0;
            }
            if (y2 < 0) {
                y2 = 20 - 1;
            }
            boolean hitSquare = false;
            boolean hitSnake = false;
            hitSquare = isHitSquare(x2, y2, hitSquare);
            hitSnake = isHitSnake(x2, y2, hitSnake);
            done = hitSquare || hitSnake;
            snakeX.add(0, x2);
            snakeY.add(0, y2);
            hitGreenSquare();
            addNextSquare();
            c.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void addNextSquare() {
        if (greenX == -1) {
            int x, y;
            boolean check1 = false;
            boolean check2 = false;
            do {
                x = r.nextInt(30);
                y = r.nextInt(20);
                check1 = isHitSquare(x, y, check1);
                check2 = isHitSnake(x, y, check2);
            } while (check2 || check1);
            greenX = x;
            greenY = y;
            int x1, y1;
            boolean check3 = false;
            boolean check4 = false;
            do {
                x1 = r.nextInt(30);
                y1 = r.nextInt(20);
                check3 = isHitSquare(x1, y1, check3);
                check4 = isHitSnake(x1, y1, check4);
            } while (check3 || check4 || greenX == x1 && greenY == y1);

            redX.add(x1);
            redY.add(y1);
        }
    }

    public void hitGreenSquare() {
        if (((snakeX.get(0) == greenX) && (snakeY.get(0) == greenY))) {
            greenX = -1;
            greenY = -1;
            setTitle("Program" + " : " + snakeX.size());
        } else {
            snakeX.remove(snakeX.size() - 1);
            snakeY.remove(snakeY.size() - 1);
        }
    }

    public boolean isHitSnake(int x2, int y2, boolean hitSnake) {
        for (int i1 = 0; i1 < snakeX.size(); i1++) {
            if ((snakeX.get(i1) == x2) && (snakeY.get(i1) == y2)) {
                if (!((snakeX.get(snakeX.size() - 1) == x2) && (snakeY.get(snakeY.size() - 1) == y2))) {
                    hitSnake = true;
                }
            }
        }
        return hitSnake;
    }

    public boolean isHitSquare(int x2, int y2, boolean hitSquare) {
        for (int i1 = 0; i1 < redX.size(); i1++) {
            if (redX.get(i1) == x2 && redY.get(i1) == y2) {
                hitSquare = true;
            }
        }
        return hitSquare;
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            paintSnakeSquares(g);
            paintGreenSquares(g, Color.green, greenX * 25, greenY * 25);
            paintRedSquares(g);
            if (done) {
                endGame(g);
            }
        }
    }

    private void endGame(Graphics g) {
        g.setColor(Color.red);
        g.setFont(new Font("Arial", Font.BOLD, 60));
        FontMetrics fm = g.getFontMetrics();
        g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
    }

    public void paintGreenSquares(Graphics g, Color green, int i, int i2) {
        g.setColor(green);
        g.fill3DRect(i + 1, i2 + 1, 25 - 2, 25 - 2, true);
    }

    public void paintSnakeSquares(Graphics g) {
        for (int i = 0; i < snakeX.size(); i++) {
            g.setColor(Color.gray);
            g.fill3DRect(snakeX.get(i) * 25 + 1, snakeY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
    }

    public void paintRedSquares(Graphics g){
        for (int i = 0; i < redX.size(); i++) {
            g.setColor(Color.red);
            g.fill3DRect(redX.get(i) * 25 + 1, redY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
    }


}