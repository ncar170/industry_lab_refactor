package ictgradschool.industry.lab_refactor.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :`(
 */
public class ExcelNew {

	private String output="";

	private ArrayList<String> firstNameList = new ArrayList<>();
	private ArrayList<String> surnameList = new ArrayList<>();

	public void start(){
		int classSize =550;
		try{
			readFirstNameFile(firstNameList);
			readSurnameFile(surnameList);
			for(int i = 1; i <= classSize; i++){
				String studentNumber = "";
				studentNumber = assignStudentNumbers(i, studentNumber);
				studentNumber = assignNamesToStudentNumber(firstNameList, surnameList, studentNumber);
				int randStudentSkill = (int)(Math.random()*101);
				studentNumber = getLabMarks(studentNumber, randStudentSkill);
				studentNumber = getTestMarks(studentNumber, randStudentSkill);
				studentNumber += "\t";
				studentNumber = getExamMarks(studentNumber, randStudentSkill);
				studentNumber += "\n";
				output += studentNumber;
			}
			printResultsFile(output);
		}
		catch(IOException e){
			System.out.println(e);
		}
	}

	public static void main(String [] args){
		ExcelNew e = new ExcelNew();
		e.start();
	}

	public String assignNamesToStudentNumber(ArrayList<String> firstNameList, ArrayList<String> surnameList, String studentNumber) {
		int randFNIndex = (int)(Math.random()*firstNameList.size());
		int randSNIndex = (int)(Math.random()*surnameList.size());
		studentNumber += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
		return studentNumber;
	}

	public String assignStudentNumbers(int i, String studentNumber) {
		if(i/10 < 1){
            studentNumber += "000" + i;
        }else if (i/100 < 1){
            studentNumber += "00" + i;
        }else if (i/1000 < 1){
            studentNumber += "0"+i;
        }else{
            studentNumber += i;
        }
		return studentNumber;
	}

	public void printResultsFile(String output) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
		bw.write(output);
		bw.close();
	}

	public String getExamMarks(String studentNumber, int randStudentSkill) {
		if(randStudentSkill <= 7){
            int randDNSProb = (int)(Math.random()*101);
            if(randDNSProb <= 5){
                studentNumber += ""; //DNS
            }else{
                studentNumber += (int)(Math.random()*40); //[0,39]
            }
        } else if(randStudentSkill <= 20){
                studentNumber += ((int)(Math.random()*10) + 40); //[40,49]
        } else if((randStudentSkill > 20) && (randStudentSkill <= 60)){
            studentNumber += ((int)(Math.random()*20) + 50);//[50,69]
        } else if((randStudentSkill > 60) && (randStudentSkill <= 90)){
            studentNumber += ((int)(Math.random()*20) + 70); //[70,89]
        } else{
            studentNumber += ((int)(Math.random()*11) + 90); //[90,100]
        }
		return studentNumber;
	}

	public String getTestMarks(String studentNumber, int randStudentSkill) {
		if(randStudentSkill <= 5){
            studentNumber += (int)(Math.random()*40); //[0,39]
        }else if((randStudentSkill > 5) && (randStudentSkill <= 20)){
                studentNumber += ((int)(Math.random()*10) + 40); //[40,49]
        } else if((randStudentSkill > 20) && (randStudentSkill <= 65)){
            studentNumber += ((int)(Math.random()*20) + 50); //[50,69]
        } else if((randStudentSkill > 65) && (randStudentSkill <= 90)){
            studentNumber += ((int)(Math.random()*20) + 70); //[70,89]
        } else{
            studentNumber += ((int)(Math.random()*11) + 90); //[90,100]
        }
		return studentNumber;
	}

	public String getLabMarks(String studentNumber, int randStudentSkill) {
		int numLabs = 3;
		for(int j = 0; j < numLabs; j++){
            if(randStudentSkill <= 5){
                studentNumber += (int)(Math.random()*40); //[0,39]
            }else if ((randStudentSkill > 5) && (randStudentSkill <= 15)){
                studentNumber += ((int)(Math.random()*10) + 40); // [40,49]
            }else if((randStudentSkill > 15) && (randStudentSkill <= 25)){
                studentNumber += ((int)(Math.random()*20) + 50); // [50,69]
            }else if((randStudentSkill > 25) && (randStudentSkill <= 65)){
                studentNumber += ((int)(Math.random()*20) + 70); // [70,89]
            } else{
                studentNumber += ((int)(Math.random()*11) + 90); //[90,100]
            }
            studentNumber += "\t";
        }
		return studentNumber;
	}

	public void readSurnameFile(ArrayList<String> surnameList) {

		try {
			String line;
			BufferedReader br = new BufferedReader(new FileReader("Surnames.txt"));
			while((line = br.readLine())!= null){
				surnameList.add(line);
			}
			br.close();
		} catch (IOException e){
			e.getMessage();
		}
	}

	public void readFirstNameFile(ArrayList<String> firstNameList) throws IOException {
		String line;
		BufferedReader br = new BufferedReader(new FileReader("FirstNames.txt"));
		while ((line = br.readLine()) != null) {
			firstNameList.add(line);
		}
		br.close();
	}
}