package ictgradschool.industry.lab_refactor.ex02;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertTrue;

/**
 * TODO Write tests
 */
public class TestExerciseTwo {

    Program program;

    @Test
    public void testGoodCode() {
        assertTrue(false);
    }

    @Before
    public void setUp(){
        program = new Program();
    }

    @Test
    public void testHitSnake(){
        boolean hit = program.isHitSnake(10, 10, false);
        assertTrue(hit);
    }

    @Test
    public void testHitSquare(){
        boolean hit = program.isHitSquare(1, 1, false);
            assertTrue(hit);
    }

//    @Test
//    public void testHitGreenSquare(){
//        program.hitGreenSquare();
//    }

    @Test
    public void testAddNewSquare(){

    }

    @Test
    public void paintGreenSquares(){

    }

}